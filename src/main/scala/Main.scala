import scala.util.{Failure, Success}

/**
  * Created by Jimmy on 21/01/2017.
  */
object Main {

  def main(args: Array[String]) {

    // This is a sample of pycall
    val pycall = new PythonCall()
    for(i <- 0 to 100000) {
      println(i)
      pycall.send("test", 1.asInstanceOf[Object], 2.asInstanceOf[Object], 4.asInstanceOf[Object]) match {
        case Success(a) =>
          println(a)
          println(a.getClass)
        case Failure(e) =>
          println(e)
      }
    }
    pycall.close()

  }
}
