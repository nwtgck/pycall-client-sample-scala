name := "call-python-in-scala"

version := "1.0"

scalaVersion := "2.11.5"

val mongoVersion = "3.4.1"

libraryDependencies ++= Seq(
  "org.mongodb" % "bson" % mongoVersion,
  "org.mongodb" % "mongo-java-driver" % mongoVersion
)